var site_url = "http://192.168.0.117/www/syk/";


var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
var $naviboolen;
var slider3;


$(document).ready(initPage);

function initPage() {
  bxSlider();
  setMapHight();
  $(window).resize(function(){
    setMapHight();
    setFullScreen();
  });
  $(window).bind('scroll', setSubNavi);
  $(window).bind('scroll', popupContnet);
  // $('.goTop > a').bind('click', goTop);
  $('.navi-toggle a').bind('click', openMenu);
  $('#current-page').bind('click', openSubMenu);
  $('.news-list').find('a').bind('click',openPopup);
  $('.report-list').find('a').bind('click',openPopup);
  $('.container-portfolio').find('.item a').bind('click',openPopup);
  $('.main-navi').find('.button-close a').bind('click', closeNavi);
  $('.popup-header').find('.icon-text a').bind('click', closeText);
  $('.awards-block .photo > img').bind('touchstart click', awardsLink);
  masonry();
  $("#current-page a").html($(".sub-navi>ul li.select a").html());
}

function openMenu() {
  if($('.page').hasClass('openNavi')){
    $('.page').removeClass('openNavi');
  } else {
    $('.page').addClass('openNavi');
  }
  if($('.header').hasClass('tiny')) {
    $('.header').removeClass('tiny');
    $naviboolen = true;5
  }
  return false;
}

function openSubMenu() {
  if($('.sub-navi').hasClass('open')){
    $('.sub-navi').removeClass('open');
  } else {
    $('.sub-navi').addClass('open');
  }
  return false;
}

function closeNavi() {
  $('.page').removeClass('openNavi');
  return false;
}

function bxSlider() {
  var init = 1;
  var totalCount = $('.bxslider2 li').length;
  $('.bxslider').bxSlider({
    auto: true,
    pause: 4000,
    autoHover: true
  });
  $('.bxslider2').bxSlider({
    pager: false,
    onSliderLoad: function(){
      $('#designercounter').html( init + ' / ' + totalCount);
    },
    onSlidePrev: function(){
      if( init > 1 ){
        init -= 1;
        $('#designercounter').html( init + ' / ' + totalCount);
      } else {
        init = totalCount;
        $('#designercounter').html( init + ' / ' + totalCount);
      }
    },
    onSlideNext: function(){
      if( init < totalCount ){
        init += 1;
        $('#designercounter').html( init + ' / ' + totalCount);
      } else {
        init = 1;
        $('#designercounter').html( init + ' / ' + totalCount);
      }
    }
  })
}

function portfolioSlider() {
  var init = 1;
  var totalCount = $('.bxslider3 li').length;
  if($('.popup-portfolio').length){
    slider3 = $('.bxslider3').bxSlider({
        // pager: false,
        // daptiveHeight: true,
        // mode: 'fade',
        onSliderLoad: function(){
          $('#portfolioCounter').html( init + ' / ' + totalCount);
          setFullScreen();
        },
        onSlidePrev: function(){
          if( init > 1 ){
            init -= 1;
            $('#portfolioCounter').html( init + ' / ' + totalCount);
          } else {
            init = totalCount;
            $('#portfolioCounter').html( init + ' / ' + totalCount);
          }
        },
        onSlideNext: function(){
          $('.popup-container .intro-text').addClass('close');
          // console.log('next');
          if( init < totalCount ){
            init += 1;
            $('#portfolioCounter').html( init + ' / ' + totalCount);
          } else {
            init = 1;
            $('#portfolioCounter').html( init + ' / ' + totalCount);
          }
        }
      });
  }
}

function setFullScreen() {
  var windowWidth  = $(window).outerWidth();
  var windowHeight = $(window).outerHeight();
  var windowProportion = windowWidth / windowHeight;
  var setImgWidth;
  var setImgHeight;
  var setMarginLeft;
  var setMarginTop;
  if($('.page').hasClass('openPopup')){
    $('.bx-viewport').css('height', windowHeight);
    $('.bx-viewport').find('li').css('height', windowHeight);
    $('.bx-viewport').find('img').each(function(){
      imgWidth = $(this).outerWidth();
      imgHeight = $(this).outerHeight();
      imgProportion = imgWidth / imgHeight;
        if( windowProportion > 0 ) { // window horizontal
          if( imgProportion > 1 ){ // horizontal
            if( (imgWidth/imgHeight) > (windowWidth/windowHeight)){
              setImgWidth = windowWidth;
              setImgHeight = windowWidth / imgWidth * imgHeight;
              setMarginTop = -(setImgHeight - windowHeight) / 2;
              setMarginLeft = 0;
            } else {
              setImgWidth = windowWidth;
              setImgHeight = windowWidth / imgWidth * imgHeight;
              setMarginLeft = 0;
              setMarginTop = -(setImgHeight - windowHeight) / 2;
            }
          } else { // vertical
            setImgHeight = windowHeight;
            setImgWidth = (windowHeight/imgHeight) * imgWidth;
            setMarginTop = 0;
            setMarginLeft = -(setImgWidth - windowWidth) / 2;
          }
          $(this).css({
            width: setImgWidth,
            height: setImgHeight,
            marginTop: setMarginTop,
            marginLeft: setMarginLeft
          });
        } else { // window vertical
          if( imgProportion > 1 ){ // horizontal
            console.log('this is horizontal');
            setImgWidth = windowWidth;
            setImgHeight = ( windowWidth / imgWidth ) * imgHeight;
            setMarginLeft = 0;
            setMarginTop = setImgHeight / 2; 
          } else { // vertical
            setImgHeight = windowHeight;
            setImgWidth = ( windowHeight / imgHeight ) * imgWidth;
            setMarginTop = 0;
            if( (imgWidth / imgHeight) > (windowWidth / windowHeight) ){
              setMarginLeft = (setImgWidth - windowWidth) / 2;
            } else {
              setMarginLeft = setImgWidth / 2;
            }
          }
          $(this).css({
            width: setImgWidth,
            height: setImgHeight,
            margintTop: setMarginTop,
            marginLeft: setMarginLeft
          });
        }
      });
} else {
  return false;
}
}

function masonry() {
  var $portfolioContainer = $('#portfoliolist');
  var $reportContainer = $('#reportlist');
  $portfolioContainer.imagesLoaded( function() {
    $portfolioContainer.masonry();
  });
  $reportContainer.imagesLoaded( function() {
    $reportContainer.masonry();
  });
}



function setSubNavi() {
  if( $(window).scrollTop() > 50) {
    $('.sub-navi').addClass('top');
  } else {
    $('.sub-navi').removeClass('top');
  }
  if( $(window).scrollTop() > 50) {
    if(!$naviboolen){
      $('.header').addClass('tiny');
    }

  }
  if( $(window).scrollTop() < 1) {
    $('.header').removeClass('tiny');
    $naviboolen = false;
  }
}

function popupContnet() {
  if( $(window).scrollTop() > 150){
    $('.popup-bottom').addClass('fixed');
    $('.popup .content-intro').addClass('fixed');
  } else{
    $('.popup-bottom').removeClass('fixed');
    $('.popup .content-intro').removeClass('fixed');
  }
    // console.log($(window).scrollTop());
  }

  function goTop() {
    $body.animate({scrollTop: 0}, 700, 'easeInOutCirc');
    return false;
  }

  function openPopup() {
    $('.page').addClass('openPopup');
    var ajax_url = $(this).attr("load-url");
    var url = $(this).attr("href");
    var site = $(this).attr("site");
    $(this).addClass("down_popup");
    change_url(ajax_url,url,site);
    return false;
  }

  function closePopup() { 
    var site = $(this).attr("site");
    console.log(site);
    $('.page').removeClass('openPopup');
    $(".popup").remove();
    if(slider3){
      slider3.destroySlider();
    }
    if(site!=""){
      window.history.pushState({},null,site);
    }
    $(window).scrollTop($(".down_popup").offset().top);
    $(this).removeClass("down_popup");
    return false;
  }

  function closeText() {
    if($('.popup-container .intro-text').hasClass('close')){
      $('.popup-container').find('.intro-text').removeClass('close');
    } 
    else {
      $('.popup-container').find('.intro-text').addClass('close');
    }
    
  }

  function awardsLink() {

    var url = $(this).closest('.awards-cover a').attr('href');
    if($(this).parent().hasClass('link')){
      debugger;
      window.location = url;
    } else {
      $(this).parent().addClass('link');
      event.stopPropagation();
      event.preventDefault();
      return false;
    }
    
  }

  function setMapHight() {
    var documentHeight = $(window).outerHeight();
    var headerHeight    = $('.header').outerHeight();
    var footerHeight    = $('.footer').outerHeight();
    mapHeight = documentHeight - headerHeight - footerHeight;
    $('#map_canvas').css('height', mapHeight);
  }

  function change_url(ajax_url,url,site){
    window.history.pushState({"url":url,"ajax":ajax_url},null,url);
    console.log(window.history);
    $.get(ajax_url+"?site="+site,function(data){
      $(".page.openPopup").append(data);
      $('.popup-bottom').find('.close-btn a').bind('click',closePopup);
      $('.popup-header').find('.close-btn a').bind('click',closePopup);
      $('.popup-header').find('.icon-text a').bind('click', closeText);
      $('.popup').find(".btn-popup a").bind("click",
        function(){
          load_ajax($(this).attr("load-url"),$(this).attr("href"),$(this).attr("site"),0);
          return false;
        });
    },"html");
  }

  function load_ajax(ajax_url,url,site,type){
    $(".popup").remove();
    if(slider3){
      slider3.destroySlider();
    }
    portfolioSlider();
    if(!type){
      window.history.pushState({"url":url,"ajax":ajax_url},null,url);
    }
    $.get(ajax_url+"?site="+site,function(data){
      $(".page.openPopup").append(data);
      $('.popup-bottom').find('.close-btn a').bind('click',closePopup);
      $('.popup-header').find('.close-btn a').bind('click',closePopup);
      $('.popup-header').find('.icon-text a').bind('click', closeText);
      $('.popup').find(".btn-popup a").bind("click",
        function(){
          load_ajax($(this).attr("load-url"),$(this).attr("href"),$(this).attr("site"),0);
          return false;
        });
    },"html");
  }
  function change_year(yy){
    if($(window).width()<768){
      $(".awards-block").hide();
      $(".awards-block[year="+yy+"]").show(300);
      $("#current-page a").html(yy);
      $(".sub-navi").removeClass("open");
    }
    else{
      $(".awards-block").show();
    }
  }

